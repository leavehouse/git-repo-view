'use strict';

// IPFS
var ipfs = window.IpfsApi('localhost', '5001');
var Unixfs = window.IpfsUnixfs;

function split_array_at(arr, i) {
    return [arr.slice(0, i), arr.slice(i+1)];
}

function split_tree_object_into_entries(xs) {
    var len_after_sep = 20;
    var curr_chunk_start = 0;
    var curr_idx = xs.indexOf(0);
    var next_chunk_start;

    var chunks = [];
    do {
        next_chunk_start = curr_idx + len_after_sep + 1;
        chunks.push(xs.slice(curr_chunk_start, next_chunk_start));
        // update current chunk info
        curr_idx = xs.indexOf(0, next_chunk_start);
        curr_chunk_start = next_chunk_start;
    } while(curr_idx !== -1);
    return chunks;
}

// from https://gist.github.com/tauzen/3d18825ae41ff3fc8981
function byteToHexString(uint8arr) {
  if (!uint8arr) {
    return '';
  }

  var hexStr = '';
  for (var i = 0; i < uint8arr.length; i++) {
    var hex = (uint8arr[i] & 0xff).toString(16);
    hex = (hex.length === 1) ? '0' + hex : hex;
    hexStr += hex;
  }

  return hexStr.toLowerCase();
}

// returns object of (<link name>, <link multihash>) pairs associated
// with MerkleDAG node `multihash`
function get_link_hashes(multihash, callback) {
    ipfs.object.links(multihash, (err, links) => {
        if(err) {
            throw err
        }
        // dirs is a dictionary where keys are names of files in bare git
        // repo, values are the multihash of the file
        var files = {}
        for(let link of links) {
            files[link.name] = link.multihash;
        }

        callback(files);
    })
}

// unmarshal the Unixfs-formatted(?) MerkleDAG node assoc. with `multihash`
function get_unixfs_file_data(multihash, callback) {
    ipfs.object.data(multihash, (err, data) => {
        if (err) {
            throw err
        }

        var unmarsheled = Unixfs.unmarshal(data);
        callback(unmarsheled.data);
    })
}

function decompress_unixfs_file_data(multihash, callback) {
    get_unixfs_file_data(multihash, obj_bytes => {
        callback(pako.inflate(obj_bytes))
    })
}

// TODO: error handling for callback
function parse_tree_obj(repo, tree_hash, callback) {
    repo.retrieve_object(tree_hash, tree_bytes => {
        // the git tree object I looked at is formatted like (using block
        // notation):
        //
        //   [tree <integer> 0 <entries in the file tree>]
        //
        // Is the "tree <integer>" part some kind of tree number? The commit
        // object had a similar "commit <integer> 0 <rest of commit info>"
        // format
        const tree_data = tree_bytes.slice(tree_bytes.indexOf(0) + 1)
        const tree_entries = split_tree_object_into_entries(tree_data)

        const tree_info = new Map([]);
        for(let entry of tree_entries) {
            const [mode_and_name, obj_hash] = split_array_at(
                entry, entry.indexOf(0))

            // further split mode_and_name
            const [mode, name] = split_array_at(mode_and_name,
                mode_and_name.indexOf(32))

            const dec = new TextDecoder("utf-8");
            const mode_text = dec.decode(mode);
            const name_text = dec.decode(name);
            const hash_text = byteToHexString(obj_hash);
            console.log([mode_text, name_text, hash_text].join(' '))
            const file_type = (mode_text[0] === '1') ? 'blob' : 'tree';
            tree_info.set(name_text, {
                type: file_type,
                hash: hash_text,
            });
        }
        callback(tree_info)
    })
}

function get_commit_tree_hash(repo, commit_hash, callback) {
    repo.retrieve_object(commit_hash, commit_bytes => {
        const commit_data = commit_bytes.subarray(
            commit_bytes.indexOf(0)+1)
        const commit_text = new TextDecoder("utf-8").decode(
            commit_data)
        console.log("commit {\n"+commit_text+"\n}")
        const commit_lines = commit_text.split('\n')
        // TODO: can the tree be in a different line? i.e.
        // do we have to iterate through all the lines and
        // check the first word of each line?
        const tree_hash = commit_lines[0].split(' ')[1]
        callback(tree_hash)
    })
}

function get_branch_tip_tree_hash(repo, branch, callback) {
    // get branch tip, which is the git-hash of a commit object
    const branch_tip_hash = repo.branch_hashes[branch]
    // get the git-hash of associated tree object from commit object
    get_commit_tree_hash(repo, branch_tip_hash, branch_tip_tree_hash => {
        callback(branch_tip_tree_hash)
    })
}


function get_subtree_multihash(tree_multihash, entry_name, callback) {
    console.log("get_subtree_multihash, tree_mh = "+Multihashes.toB58String(tree_multihash)+", entry_name = "+entry_name);
    get_link_hashes(tree_multihash, suffix_files => {
        console.log("get_subtree_multihash > get_link_hashes", suffix_files, entry_name);
        callback(suffix_files[entry_name])
    })
}

function resolve_tree_path(repo, tree_hash, path, callback) {
    path = path || '';
    console.log("resolve_tree_path entrance", tree_hash, '.'+path+'.');

    if(path !== '') {
        const first_sep = path.indexOf('/')
        const [next_path_comp, rem_path] = first_sep === -1
            ? [path, '']
            : split_array_at(path, first_sep);
        console.log("resolve_tree_path, next_path_comp = "+next_path_comp+", rem_path = "+rem_path);
        parse_tree_obj(repo, tree_hash, parsed_tree => {
            const subtree_hash = parsed_tree.get(next_path_comp).hash
            resolve_tree_path(repo, subtree_hash, rem_path, callback)
        })
    } else {
        console.log("resolve_tree_path, path is empty");
        return callback(tree_hash);
    }
}

// pseudocode
//   now get (over IPFS) the (legacy) DagNode of the git tree object
//      corresponding to the tree object hash
//   the git tree object is array of filetree entries, each having a name, a
//      mode, and the git-hash of the git object (tree or blob?) it points to.
//   return a Map where keys are names, values are { hash, obj_type }
function lookup_tree_path(repo, branch, path, callback) {
    console.log("lookup_tree_path", branch, path);
    get_branch_tip_tree_hash(repo, branch, branch_tip_tree_hash => {
        resolve_tree_path(repo, branch_tip_tree_hash, path, tree_hash => {
            console.log("resolved path = "+path+" for tree = "+branch_tip_tree_hash+", resulting tree hash = "+tree_hash);
            parse_tree_obj(repo, tree_hash, parsed_tree => {
                callback(parsed_tree)
            })
        })
    })
}

// `parsed_tree` is a Map where keys are filetree object name strings,
// values are {type: <obj type>, hash: <git obj hash>} objects
// Returns: Array of filetree entries, to be used as part of the data of the
// Tree view component
function viewify_parsed_tree_obj(parsed_tree) {
    const [ui_trees, ui_blobs] = [[], []];
    for(let [name, entry] of parsed_tree) {
        const ui_entry_info = {
            name: name,
            type: entry.type
        }
        if(entry.type === 'tree') {
            ui_trees.push(ui_entry_info)
        } else {
            ui_blobs.push(ui_entry_info)
        }
    }

    // Build the
    const tree_ui_info = [];
    const num_trees = ui_trees.length;
    for(let [id, tree] of ui_trees.entries()) {
        tree['id'] = id;
        tree_ui_info.push(tree);
    }
    for(let [id, blob] of ui_blobs.entries()) {
        blob['id'] = num_trees + id;
        tree_ui_info.push(blob);
    }
    return tree_ui_info;
}

function lookup_tree_path_for_view(repo, branch, path, cb) {
    lookup_tree_path(repo, branch, path, parsed_tree => {
        cb(null, viewify_parsed_tree_obj(parsed_tree))
    })
}

function branch_hashes_from_packed_refs(refs_lines) {
    const branch_hashes = {};
    for(let line of refs_lines) {
        if(line[0] !== '#' && line !== '') {
            const [branch_tip, branch_ref_name] = line.split(' ');
            if(branch_ref_name.indexOf('refs/heads/') === 0) {
                const branch_name = branch_ref_name.replace('refs/heads/', '');
                branch_hashes[branch_name] = branch_tip;
            }
        }
    }
    return branch_hashes;
}

class Repo {
    constructor(multihash, init_view) {
        this.multihash = multihash;
        get_link_hashes(this.multihash, files => {
            this.repo_dir = files;

            // Set up this.branch_hashes
            get_unixfs_file_data(this.repo_dir['packed-refs'], refs => {
                const refs_lines = refs.toString().split("\n");
                this.branch_hashes = branch_hashes_from_packed_refs(refs_lines)

                // Set up this.obj_prefix_folders
                get_link_hashes(this.repo_dir['objects'], prefix_files => {
                    this.obj_prefix_folders = prefix_files;
                    init_view(this)
                })
            })

        })
    }

    get_object_multihash(obj_hash, callback) {
        console.log("get_object_multihash", obj_hash);
        const [hash_pre, hash_rem] = [obj_hash.slice(0, 2), obj_hash.slice(2)];
        const pre_folder_tree_mh = this.obj_prefix_folders[hash_pre];
        console.log("the prefix folder mh = "+Multihashes.toB58String(pre_folder_tree_mh));
        //console.log("get_object_multihash, hash_pre = "+hash_pre+", hash_rem = "+hash_rem);
        get_subtree_multihash(pre_folder_tree_mh, hash_rem, obj_mh => {
            console.log("get_object_multihash after get_subtree_multihash, obj_mh = "+Multihashes.toB58String(obj_mh));
            callback(obj_mh)
        })
    }

    /*
    retrieve_object(obj_hash, callback) {
        get_object_multihash(obj_hash, obj_multihash => {
            get_unixfs_file_data(obj_multihash, blob => {
                callback(pako.inflate(blob))
            })
        })
    }
    */

    // TODO: deduplicate with get_object_multihash?
    retrieve_object(obj_hash, callback) {
        const [hash_pre, hash_rem] = [obj_hash.slice(0, 2), obj_hash.slice(2)];
        const pre_folder_tree_mh = this.obj_prefix_folders[hash_pre];
        console.log("retrieve_object, hash="+obj_hash);
        get_subtree_multihash(pre_folder_tree_mh, hash_rem, obj_mh => {
            console.log("retrieved, obj_mh="
                        + Multihashes.toB58String(obj_mh));
            decompress_unixfs_file_data(obj_mh, obj_bytes => {
                callback(obj_bytes)
            });
        });
    }
}

function init_view(repo) {
    console.log("init_view");
    Vue.component('filetree-path', {
        props: ['branch', 'path'],
        template: '#filetree-path-template'
    });

    Vue.component('filetree-entry', {
        props: ['entry', 'branch', 'path'],
        template: '#tree-entry-template',
        methods: {
            is_tree: function(entry) {
                return entry.type == 'tree';
            },
            entry_link: function(entry, branch, path) {
                const link_components = [entry.type, branch, entry.name];
                console.log(link_components)
                if(path) {
                    link_components.splice(2, 0, path);
                }
                return '/'+link_components.join('/');
            },
        }
    });

    const Tree = {
        props: ['branch', 'path'],
        template: '#tree-template',
        data: function() {
            return {
                loading: false,
                error: null,
                tree: null
            }
        },
        created: function() {
            this.fetchData()
        },
        watch: {
            '$route': 'fetchData'
        },
        methods: {
            fetchData: function() {
                this.error = this.tree = null
                this.loading = true
                lookup_tree_path_for_view(repo, this.$route.params.branch,
                                this.$route.params.path, (err, tree) => {
                    this.loading = false
                    if(err) {
                        this.error = err
                    } else {
                        this.tree = tree
                    }
                })
            }
        }
    }

    const Blob = {
        props: ['branch', 'path'],
        template: '#blob-template'
    }

    const routes = [
      { path: '/', redirect: '/tree/master' },
      { path: '/tree/:branch/:path*', component: Tree, props: true},
      { path: '/blob/:branch/:path*', component: Blob, props: true }
    ]

    const router = new VueRouter({
      routes // short for `routes: routes`
    })

    const app = new Vue({
      router
    }).$mount('#app')

    return app;
}

const repo_multihash = 'QmVqEbRodYDPyzRAZAS6bpgfnqu7vy5edEduw2BrxDjiR8';
new Repo(repo_multihash, init_view)
